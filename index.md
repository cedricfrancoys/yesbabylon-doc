# YesBabylon Servers

This documentation presents YesBabylon Server principles and its implementation.

* The virtualisation strategy and its benefits
* The account strategy for managing apps on the server
* How to install and administrate such server.
